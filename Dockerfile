FROM node:8

WORKDIR /app

COPY . /app

RUN yarn install --production

EXPOSE 3050

CMD ["node", "/app/index.js"]