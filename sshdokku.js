const fs = require('fs')
const path = require('path')
const node_ssh = require('node-ssh')
const ssh = new node_ssh()

const command = 'dokku apps'

async function task() {
	await ssh.connect({
		host: 'localhost',
		username: 'dokku',
		privateKey: '/app/circle_keys/id_rsa'
	})
	let result = await ssh.execCommand(command)
	console.log(result)
}

task()
